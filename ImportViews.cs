﻿using CelularDireto.Collections.Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimParsers.Domain;
using TimParsers.POCO.TCC_TelaDoCarrinhoView;
using TimParsers.POCO.TCC_TelaInicialView;
using TimParsers.POCO.TCC_TelaRelacionamentoView;

namespace TimParsers
{
	public static class ImportViews
	{
		public static void ImportTelaInicialView(MultDictionary<string, string[]> data, SiebelContext domain)
		{
			var telaInicialView = new TCCTelaInicialView(data.Values.ToList());
			if (telaInicialView.ClienteAcesso.Count() == 0) return;
			domain.Cliente.Import(telaInicialView.ClienteAcesso.FirstOrDefault().DadosCliente);
			domain.Acesso.Import(telaInicialView.ClienteAcesso.FirstOrDefault().DadosAcesso);

			foreach (var item in telaInicialView.OS)
			{
				var domainOS = domain.OS.FirstOrDefault(x => x.RowId == item.RowId);
				if (domainOS == null)
				{
					domainOS = new TimParsers.Domain.OS();
					domainOS.Import(item);
					domain.OS.Add(domainOS);
				}
				else
					domainOS.Import(item);
			}

			foreach (var item in telaInicialView.ReqServico)
			{
				var domainRServ = domain.ReqServico.FirstOrDefault(x => x.RowId == item.RowId);
				if (domainRServ == null)
				{
					domainRServ = new TimParsers.Domain.ReqServico();
					domainRServ.Import(item);
					domain.ReqServico.Add(domainRServ);
				}
				else
					domainRServ.Import(item);
			}
		}

		public static void ImportTelaRelacionamentoView(MultDictionary<string, string[]> data, SiebelContext domain)
		{
			var telaRelacionamentoView = new TCCTelaRelacionamentoView(data.Values.ToList());
			if (telaRelacionamentoView.Cliente.Count() == 0) return;
			domain.Cliente.Import(telaRelacionamentoView.Cliente.FirstOrDefault());

			foreach (var item in telaRelacionamentoView.DadosInteracaoAlcada)
			{
				var domainDia = domain.DadosInteracaoAlcada.FirstOrDefault(x => x.RowId == item.RowId);
				if (domainDia == null)
				{
					domainDia = new TimParsers.Domain.DadosInteracaoAlcada();
					domainDia.Import(item);
					domain.DadosInteracaoAlcada.Add(domainDia);
				}
				else
					domainDia.Import(item);
			}

			foreach (var item in telaRelacionamentoView.LinhasCliente)
			{
				var domainLinhasCliente = domain.LinhasCliente.FirstOrDefault(x => x.RowId == item.RowId);
				if (domainLinhasCliente == null)
				{
					domainLinhasCliente = new TimParsers.Domain.LinhasCliente();
					domainLinhasCliente.Import(item);
					domain.LinhasCliente.Add(domainLinhasCliente);
				}
				else
					domainLinhasCliente.Import(item);
			}

			foreach (var item in telaRelacionamentoView.Ordens)
			{
				var domainOrdens = domain.Ordens.FirstOrDefault(x => x.RowId == item.RowId);
				if (domainOrdens == null)
				{
					domainOrdens = new TimParsers.Domain.Ordens();
					domainOrdens.Import(item);
					domain.Ordens.Add(domainOrdens);
				}
				else
					domainOrdens.Import(item);
			}

			foreach (var item in telaRelacionamentoView.Proponi)
			{
				var domainProponi = domain.Proponi.FirstOrDefault(x => x.RowId == item.RowId);
				if (domainProponi == null)
				{
					domainProponi = new TimParsers.Domain.Proponi();
					domainProponi.Import(item);
					domain.Proponi.Add(domainProponi);
				}
				else
					domainProponi.Import(item);
			}
		}

		public static void ImportTelaCarrinhoView(MultDictionary<string, string[]> data, SiebelContext domain)
		{
			var telaCarrinhoView = new TCCTelaDoCarrinhoView(data.Values.ToList());
			if (telaCarrinhoView.DadosCliente.Count() == 0) return;
			domain.Cliente.Import(telaCarrinhoView.DadosCliente.FirstOrDefault());

			foreach (var item in telaCarrinhoView.AtributosItem)
			{
				var domainAtributos = domain.AtributosItem.FirstOrDefault(x => x.RowId == item.RowId);
				if (domainAtributos == null)
				{
					domainAtributos = new TimParsers.Domain.AtributosItem();
					domainAtributos.Import(item);
					domain.AtributosItem.Add(domainAtributos);
				}
				else
					domainAtributos.Import(item);
			}

			foreach (var item in telaCarrinhoView.BilhetesPortabilidade)
			{
				var domainBilPort = domain.BilhetesPortabilidade.FirstOrDefault(x => x.RowId == item.RowId);
				if (domainBilPort == null)
				{
					domainBilPort = new TimParsers.Domain.BilhetesPortabilidade();
					domainBilPort.Import(item);
					domain.BilhetesPortabilidade.Add(domainBilPort);
				}
				else
					domainBilPort.Import(item);
			}

			foreach (var item in telaCarrinhoView.ItensOrdem)
			{
				var domainItensOrdens = domain.ItensOrdem.FirstOrDefault(x => x.RowId == item.RowId);
				if (domainItensOrdens == null)
				{
					domainItensOrdens = new TimParsers.Domain.ItensOrdem();
					domainItensOrdens.Import(item);
					domain.ItensOrdem.Add(domainItensOrdens);
				}
				else
					domainItensOrdens.Import(item);
			}

			foreach (var item in telaCarrinhoView.Ordem)
			{
				var domainOrdens = domain.Ordens.FirstOrDefault(x => x.RowId == item.RowId);
				if (domainOrdens == null)
				{
					domainOrdens = new TimParsers.Domain.Ordens();
					domainOrdens.Import(item);
					domain.Ordens.Add(domainOrdens);
				}
				else
					domainOrdens.Import(item);
			}
		}
	}
}
