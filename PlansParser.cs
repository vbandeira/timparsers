﻿using TimParsers.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimParsers
{
	public static class PlansParser
	{
		public static List<Plano> ParseData(string rawData)
		{
			var data = rawData.Substring(rawData.IndexOf("ExcludedModel")).Split('%');
			var DisplayDesc = QueryValue(data, "Display Description");
			var ProductId = QueryValue(data, "Product Id");
			var PortItem = QueryValue(data, "Port Item Id");
			var CxOrigId = QueryValue(data, "CxOrigID");
			var CxObjName = QueryValue(data, "CxObjName");

			var result = new List<Plano>();

			for(var i = 0; i < DisplayDesc.Count(); i++)
			{
				var plan = new Plano
				{
					DisplayDescription = DisplayDesc[i],
					ProductId = ProductId[i],
					PortItemId = PortItem[i],
					CxObjName = CxObjName[i],
					CxOrigId = CxOrigId[i]
				};
				result.Add(plan);
			}

			return result;
		}

		

		private static List<string> QueryValue(string[] data, string KeyToSearch, bool OnlyChars = false)
		{
			var idxs = data.Select((x, i) => (x == KeyToSearch ? i + 1 : -1)).Where(x => x >= 0).ToList();
			if (idxs.Count == 0) return null;

			return !OnlyChars ? data.Select((x, i) => idxs.Contains(i) ? x : null).Where(x => x != null).ToList() : data.Select((x, i) => idxs.Contains(i) ? x : null).Where(x => x != null && !x.All(char.IsDigit)).ToList();
		}

	}
}
