﻿using TimParsers.POCO;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TimParsers.POCO.TCC_TelaInicialView;
using TimParsers.POCO.TCC_TelaRelacionamentoView;
using TimParsers.POCO.TCC_TelaDoCarrinhoView;
using CelularDireto.Collections.Generic;
using System.Reflection;
using System.IO;
using Newtonsoft.Json;

namespace TimParsers._NUnitTests
{
	[TestFixture, Category("Parser")]
	public class Tests
	{
		[TestCase]
		public void TestImport()
		{
			var acessoResult = new POCO.TCC_TelaInicialView.ClienteAcesso._dadosAcesso
			{
				NumAcesso = "21999999999"
			};

			var acessoDomain = new Domain.Acesso();

			var cliente = new POCO.TCC_TelaInicialView.ClienteAcesso._dadosCliente
			{
				CPFCNPJ = "123456789-01"
			};

			try
			{
				acessoDomain.Import(cliente);
				Assert.Fail();
			}
			catch (ArgumentException)
			{
				//Expected situation. Nothing to do.
			}
			catch (Exception)
			{
				Assert.Fail();
			}
			
			Assert.True(acessoDomain.Import(acessoResult));
		}

		[TestCase]
		public void WorkflowTest()
		{
			var path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
			var files = System.IO.Directory.GetFiles(Path.Combine(path, "..\\..", "_NUnitTests", "Resources"));

			var domain = new Domain.SiebelContext();

			foreach(var f in files)
			{
				var rawData = File.ReadAllText(f);
				var data = SiebelParser.DecodeData(rawData, new List<string> { "View", "v" });
				
				switch (data["View"].FirstOrDefault())
				{
					case "TCC_TelaInicialView":
						ImportViews.ImportTelaInicialView(data, domain);
						break;
					case "TCC_TelaRelacionamentoView":
						ImportViews.ImportTelaRelacionamentoView(data, domain);
						break;
					case "TCC_TelaDoCarrinhoView":
						ImportViews.ImportTelaCarrinhoView(data,  domain);
						break;
				}
			}

			Assert.True(true);
		}

		[TestCase]
		public void TestParserPlans()
		{
			var rawdata = File.ReadAllText(@"C:\Users\vinicius.bandeira\Desktop\Siebel\Fiddler\Ativacao\1\raw\responses\136_s.txt");
			var plans = PlansParser.ParseData(rawdata);
			File.WriteAllText(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "plans.json"), JsonConvert.SerializeObject(plans));
		}

		
	}
}
