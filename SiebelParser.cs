﻿using CelularDireto.Collections.Generic;
using TimParsers.POCO.TCC_TelaInicialView;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TimParsers
{
	public class SiebelParser
	{
		readonly string siebelData = System.IO.File.ReadAllText("C:\\Users\\vinicius.bandeira\\Desktop\\Siebel\\siebelData.txt");

		private static List<string> ParseValues(string inValue, bool print = false)
		{
			var sb = new StringBuilder();
			var empty = true;

			var vals = inValue;
			var tempVal = vals;
			var listVals = new List<string>();

			while (!string.IsNullOrEmpty(tempVal))
			{
				var marker = tempVal.IndexOf('*');
				int lenValue;
				string value;

				if (marker > 0)
				{
					lenValue = Convert.ToInt16(tempVal.Substring(0, marker));
					value = tempVal.Substring(marker + 1, lenValue);
					tempVal = tempVal.Substring(lenValue + marker + 1);
				}
				else
				{
					value = tempVal;
					lenValue = 0;
					tempVal = "";
				}

				if (lenValue > 0)
					empty = false;

				listVals.Add(value);
			}

			if (!empty)
			{
				sb.AppendLine($"{{ { string.Join(",", listVals.ToArray()) } }}");
				if (print)
					Console.WriteLine(sb.ToString());

				return listVals;
			}
			else
				return null;
		}

		private static List<string> QueryValue(string[] data, string KeyToSearch, bool OnlyChars = false)
		{
			var idxs = data.Select((x, i) => (x == KeyToSearch ? i + 1 : -1)).Where(x => x >= 0).ToList();
			if (idxs.Count == 0) return null;

			return !OnlyChars ? data.Select((x, i) => idxs.Contains(i) ? x : null).Where(x => x != null).ToList() : data.Select((x, i) => idxs.Contains(i) ? x : null).Where(x => x != null && !x.All(char.IsDigit)).ToList();
		}

		public static MultDictionary<string, string[]> DecodeData(string data, List<string> keys)
		{
			var text = data.Substring(data.IndexOf('@'));
			var arrs = text.Split(new string[] { "0`1`", "`" }, StringSplitOptions.None);

			var siebelData = new MultDictionary<string, string[]>();
			foreach (var k in keys)
			{
				var result = QueryValue(arrs, k, true);
				if (result == null) continue;
				foreach (var r in result)
				{
					if (r.Contains('*'))
					{
						if (r.Contains("TCC_")) continue;
						if (r == null) continue;
						var value = ParseValues(r);
						if (value == null) continue;
						siebelData.Add(new KeyValuePair<string, string[]>(k, value.ToArray()));
					}
					else
						siebelData.Add(new KeyValuePair<string, string[]>(k, new string[] { r }));
				}
			}

			return siebelData;
		}
	}
}
