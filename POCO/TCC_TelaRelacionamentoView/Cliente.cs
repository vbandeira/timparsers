﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimParsers.POCO.TCC_TelaRelacionamentoView
{
	public class Cliente: ICliente
	{
		public string TipoCliente { get; set; }
		public string TipoDocumento { get; set; }
		public string TelefoneContato { get; set; }
		public string PossuiDeficiencia { get; set; }
		public string CPFCNPJ { get; set; }
		public string NumeroDocumento { get; set; }
		public string Endereco { get; set; }
		public string Titular { get; set; }
		public string DataNascimento { get; set; }
		public string Status { get; set; }
		public string NomeMae { get; set; }
		public string Sexo { get; set; }
		public string MarcacaoSobAnalise { get; set; }
		public string RowId { get; set; }

		public Cliente() { }

		public Cliente(string[] values)
		{
			TipoCliente = values[0];
			TipoDocumento = values[1];
			TelefoneContato = values[2];
			PossuiDeficiencia = values[3];
			CPFCNPJ = values[4];
			NumeroDocumento = values[5];
			Endereco = values[6];
			Titular = values[7];
			DataNascimento = values[8];
			Status = values[9];
			NomeMae = values[10];
			Sexo = values[11];
			MarcacaoSobAnalise = values[12];
			RowId = values[13];
		}
	}
}
