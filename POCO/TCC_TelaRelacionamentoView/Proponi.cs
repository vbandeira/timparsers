﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimParsers.POCO.TCC_TelaRelacionamentoView
{
	public class Proponi
	{
		public string Oferta { get; set; }
		public string NumeroLinha { get; set; }
		public string StatusProponi { get; set; }
		public string DataInicio { get; set; }
		public string DataFim { get; set; }
		public string Produtos { get; set; }
		public string DescricaoCampanha { get; set; }
		public string CodigoCampanha { get; set; }
		public string DescricaoPrograma { get; set; }
		public string TipoCampanha { get; set; }
		public string DataAlteracao { get; set; }
		public string CodigoPDV { get; set; }
		public string Login { get; set; }
		public string StatusOferta { get; set; }
		public string RowId { get; set; }

		public Proponi() {}

		public Proponi(string[] values)
		{
			Oferta = values[0];
			NumeroLinha = values[1];
			StatusProponi = values[2];
			DataInicio = values[3];
			DataFim = values[4];
			Produtos = values[5];
			DescricaoCampanha = values[6];
			CodigoCampanha = values[7];
			DescricaoPrograma = values[8];
			TipoCampanha = values[9];
			DataAlteracao = values[10];
			CodigoPDV = values[11];
			Login = values[12];
			StatusOferta = values[12];
			RowId = values[13];
		}
	}
}
