﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimParsers.POCO.TCC_TelaRelacionamentoView
{
	public class DadosInteracaoAlcada
	{
		public string CodigoInteracao { get; set; }
		public string CriadoEm { get; set; }
		public string Tipo { get; set; }
		public string Status { get; set; }
		public string FlagPendencia { get; set; }
		public string DataHoraSolicitacao { get; set; }
		public string LoginSolicitante { get; set; }
		public string JustificativaSolicitante { get; set; }
		public string StatusSolicitacao { get; set; }
		public string NotasSolicitante { get; set; }
		public string DataLimiteAprovacao { get; set; }
		public string LoginAprovador { get; set; }
		public string NomeResponsavelAprovacao { get; set; }
		public string RowId { get; set; }

		public DadosInteracaoAlcada() {}

		public DadosInteracaoAlcada(string[] values)
		{
			CodigoInteracao = values[0];
			CriadoEm = values[1];
			Tipo = values[2];
			Status = values[3];
			FlagPendencia = values[4];
			DataHoraSolicitacao = values[5];
			LoginSolicitante = values[6];
			JustificativaSolicitante = values[7];
			StatusSolicitacao = values[8];
			NotasSolicitante = values[9];
			DataLimiteAprovacao = values[10];
			LoginAprovador = values[11];
			NomeResponsavelAprovacao = values[12];
			RowId = values[13];
		}
	}
}
