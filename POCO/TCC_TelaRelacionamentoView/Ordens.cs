﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimParsers.POCO.TCC_TelaRelacionamentoView
{
	public class Ordens: IOrdem
	{
		public string NumeroOrdem { get; set; }
		public string StatusOrdem { get; set; }
		public string PDVResponsavel { get; set; }
		public string LoginResponsavel { get; set; }
		public string ValorPagar { get; set; }
		public string ProcuradorRepresentanteLegal { get; set; }
		public string Data { get; set; }
		public string DataUltimaAtualizacao { get; set; }
		public string RowId { get; set; }

		public Ordens()	{}

		public Ordens(string[] values)
		{
			NumeroOrdem = values[0];
			StatusOrdem = values[1];
			PDVResponsavel = values[2];
			LoginResponsavel = values[3];
			ValorPagar = $"{values[4]} {values[5]}" ;
			ProcuradorRepresentanteLegal = values[6];
			Data = values[7];
			DataUltimaAtualizacao = values[8];
			RowId = values[9];
		}
	}
}
