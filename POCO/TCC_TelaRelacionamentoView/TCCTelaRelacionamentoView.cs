﻿using TimParsers.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace TimParsers.POCO.TCC_TelaRelacionamentoView
{
	/// <summary>
	/// TCCTelaRelacionamentoView class that contains informations of this view
	/// </summary>
	public class TCCTelaRelacionamentoView
	{
		public List<Cliente> Cliente { get; set; } = new List<Cliente>();
		public List<LinhasCliente> LinhasCliente { get; set; } = new List<LinhasCliente>();
		public List<Ordens> Ordens { get; set; } = new List<Ordens>();
		public List<DadosInteracaoAlcada> DadosInteracaoAlcada { get; set; } = new List<DadosInteracaoAlcada>();
		public List<Proponi> Proponi { get; set; } = new List<Proponi>();

		public TCCTelaRelacionamentoView(List<string[]> data)
		{
			foreach (var item in data)
			{
				switch (item.Count().ToString())
				{
					case "14":
						{
							//Test type: Cliente, DadosInteracaoAlcada or Proponi
							if (Util.validarCPF(item[4]))
							{
								Cliente.Add(new TCC_TelaRelacionamentoView.Cliente(item));
								break;
							}
							if (Regex.IsMatch(item[2], "[0-9]{10}"))
							{
								Proponi.Add(new TCC_TelaRelacionamentoView.Proponi(item));
								break;
							}
							DadosInteracaoAlcada.Add(new TCC_TelaRelacionamentoView.DadosInteracaoAlcada(item));
							break;
						}
					case "17":
						{
							//LinhasCliente
							LinhasCliente.Add(new TCC_TelaRelacionamentoView.LinhasCliente(item));
							break;
						}
					case "10":
						{
							//Ordens
							Ordens.Add(new TCC_TelaRelacionamentoView.Ordens(item));
							break;
						}
					default:
						{
							break;
						}
				}
			}
		}
	}
}
