﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimParsers.POCO.TCC_TelaRelacionamentoView
{
	public class LinhasCliente
	{
		public string TimChipICCID { get; set; }
		public string Numero { get; set; }
		public string DescricaoProduto { get; set; }
		public string TipoProduto { get; set; }
		public string NumeroFixoAlternativo { get; set; }
		public string Plano { get; set; }
		public string Status { get; set; }
		public string DataAtivacao { get; set; }
		public string PDVAtivacao { get; set; }
		public string Acao { get; set; }
		public string Motivo { get; set; }
		public string MotivoAcaoAnterior { get; set; }
		public string RowId { get; set; }
		public string Unknown1 { get; set; }
		public string Unknown2 { get; set; }
		public string Unknown3 { get; set; }
		public string Unknown4 { get; set; }

		public LinhasCliente()	{}

		public LinhasCliente(string[] values)
		{
			TimChipICCID = values[0];
			Numero = values[1];
			DescricaoProduto = values[2];
			TipoProduto = values[3];
			NumeroFixoAlternativo = values[4];
			Plano = values[5];
			Status = values[6];
			DataAtivacao = values[7];
			PDVAtivacao = values[8];
			Acao = values[9];
			Motivo = values[10];
			MotivoAcaoAnterior = values[11];
			RowId = values[12];
			Unknown1 = values[13];
			Unknown2 = values[14];
			Unknown3 = values[15];
			Unknown4 = values[16];
		}
	}
}
