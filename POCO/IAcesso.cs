﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimParsers.POCO
{
	interface IAcesso
	{
		string NumAcesso { get; set; }
		string Status { get; set; }
	}
}
