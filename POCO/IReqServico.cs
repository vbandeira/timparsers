﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimParsers.POCO
{
	interface IReqServico
	{
		string Protocolo { get; set; }
		string RowId { get; set; }
	}
}
