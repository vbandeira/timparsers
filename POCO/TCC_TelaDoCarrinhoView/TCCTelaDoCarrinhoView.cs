﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimParsers.POCO.TCC_TelaDoCarrinhoView
{
	public class TCCTelaDoCarrinhoView
	{
		public List<Ordem> Ordem { get; set; } = new List<TCC_TelaDoCarrinhoView.Ordem>();
		public List<ItensOrdem> ItensOrdem { get; set; } = new List<TCC_TelaDoCarrinhoView.ItensOrdem>();
		public List<DadosCliente> DadosCliente { get; set; } = new List<TCC_TelaDoCarrinhoView.DadosCliente>();
		public List<AtributosItem> AtributosItem { get; set; } = new List<TCC_TelaDoCarrinhoView.AtributosItem>();
		public List<BilhetesPortabilidade> BilhetesPortabilidade { get; set; } = new List<TCC_TelaDoCarrinhoView.BilhetesPortabilidade>();

		public TCCTelaDoCarrinhoView(List<string[]> data)
		{
			foreach(var item in data)
			{
				switch(item.Count().ToString())
				{
					case "3":
						AtributosItem.Add(new TCC_TelaDoCarrinhoView.AtributosItem(item));
						break;
					case "34":
						BilhetesPortabilidade.Add(new TCC_TelaDoCarrinhoView.BilhetesPortabilidade(item));
						break;
					case "8":
						DadosCliente.Add(new TCC_TelaDoCarrinhoView.DadosCliente(item));
						break;
					case "23":
						ItensOrdem.Add(new TCC_TelaDoCarrinhoView.ItensOrdem(item));
						break;
					case "27":
						Ordem.Add(new TCC_TelaDoCarrinhoView.Ordem(item));
						break;
					default:
						break;
				}
			}
		}
	}
}
