﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimParsers.POCO.TCC_TelaDoCarrinhoView
{
	public class BilhetesPortabilidade
	{
		public string BilheteTemporario { get; set; }
		public string BilhetePortabilidade { get; set; }
		public string LinhaTemporaria { get; set; }
		public string LinhaPortada { get; set; }
		public string StatusBilhete { get; set; }
		public string TipoLinha { get; set; }
		public string NomeCliente { get; set; }
		public string NumeroDocumento { get; set; }
		public string TelefoneContato { get; set; }
		public string OperadoraDoadora { get; set; }
		public string DataPortabilidade { get; set; }
		public string BuscaData { get; set; }
		public string DataSolicitacao { get; set; }
		public string DataReagendamento { get; set; }
		public string DataDesistencia { get; set; }
		public string DataReal { get; set; }
		public string MotivoCancelamento { get; set; }
		public string CEP { get; set; }
		public string CNL { get; set; }
		public string TipoLogradouro { get; set; }
		public string Logradouro { get; set; }
		public string Cidade { get; set; }
		public string Bairro { get; set; }
		public string Estado { get; set; }
		public string Complemento { get; set; }
		public string MotivoRecusa { get; set; }
		public string AutenticacaoEA { get; set; }
		public string FraudeErro { get; set; }
		public string BilheteFraudeErro { get; set; }
		public string JustificativaBilheteFraudeErro { get; set; }
		public string Surfing { get; set; }
		public string StatusPortabilidade { get; set; }
		public string TipoPortabilidade { get; set; }
		public string RowId { get; set; }

		public BilhetesPortabilidade() {}

		public BilhetesPortabilidade(string[] values)
		{
			BilheteTemporario = values[0];
			BilhetePortabilidade = values[1];
			LinhaTemporaria = values[2];
			LinhaPortada = values[3];
			StatusBilhete = values[4];
			TipoLinha = values[5];
			NomeCliente = values[6];
			NumeroDocumento = values[7];
			TelefoneContato = values[8];
			OperadoraDoadora = values[9];
			DataPortabilidade = values[10];
			BuscaData = values[11];
			DataSolicitacao = values[12];
			DataReagendamento = values[13];
			DataDesistencia = values[14];
			DataReal = values[15];
			MotivoCancelamento = values[16];
			CEP = values[17];
			CNL = values[18];
			TipoLogradouro = values[19];
			Logradouro = values[20];
			Cidade = values[21];
			Bairro = values[22];
			Estado = values[23];
			Complemento = values[24];
			MotivoRecusa = values[25];
			AutenticacaoEA = values[26];
			FraudeErro = values[27];
			BilheteFraudeErro = values[28];
			JustificativaBilheteFraudeErro = values[29];
			Surfing = values[30];
			StatusPortabilidade = values[31];
			TipoPortabilidade = values[32];
			RowId = values[33];
		}
	}
}
