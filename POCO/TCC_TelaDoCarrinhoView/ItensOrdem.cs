﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimParsers.POCO.TCC_TelaDoCarrinhoView
{
	public class ItensOrdem
	{
		public string Unknown1 { get; set; }
		public string Unknown2 { get; set; }
		public string Unknown3 { get; set; }
		public string NomeProduto { get; set; }
		public string Acao { get; set; }
		public string NumeroAtual { get; set; }
		public string StatusItem { get; set; }
		public string TaxaAdesao { get; set; }
		public string AcoesSolicitadas { get; set; }
		public string BilhetePortabilidadeTemporario { get; set; }
		public string LinhaPortada { get; set; }
		public string StatusPortabilidade { get; set; }
		public string FlagAlcada { get; set; }
		public string NovoTIMChip { get; set; }
		public string TIMChipAtual { get; set; }
		public string NumeroASerDivulgado { get; set; }
		public string EnderecoInstalacao { get; set; }
		public string RowId { get; set; }
		public string Unknown5 { get; set; }
		public string Unknown6 { get; set; }
		public string Unknown7 { get; set; }
		public string Unknown8 { get; set; }

		public ItensOrdem()	{}

		public ItensOrdem(string[] values)
		{
			Unknown1 = values[0];
			Unknown2 = values[1];
			Unknown3 = values[2];
			NomeProduto = values[3];
			Acao = values[4];
			NumeroAtual = values[5];
			StatusItem = values[7];
			TaxaAdesao = values[8];
			AcoesSolicitadas = values[6];
			BilhetePortabilidadeTemporario = values[9];
			LinhaPortada = values[10];
			StatusPortabilidade = values[11];
			FlagAlcada = values[12];
			NovoTIMChip = values[13];
			TIMChipAtual = values[14];
			NumeroASerDivulgado = values[16];
			EnderecoInstalacao = values[17];
			RowId = values[18];
			Unknown5 = values[19];
			Unknown6 = values[20];
			Unknown7 = values[21];
			Unknown8 = values[22];
		}
	}
}
