﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimParsers.POCO.TCC_TelaDoCarrinhoView
{
	public class DadosCliente:ICliente
	{
		public string Titular { get; set; }
		public string CPFCNPJ { get; set; }
		public string TipoDocumento { get; set; }
		public string Endereco { get; set; }
		public string TipoCliente { get; set; }
		public string Status { get; set; }
		public string NumeroDocumento { get; set; }
		public string RowId { get; set; }

		public DadosCliente() {	}

		public DadosCliente(string[] values)
		{
			Titular = values[0];
			CPFCNPJ = values[1];
			TipoDocumento = values[2];
			Endereco = values[3];
			TipoCliente = values[4];
			Status = values[5];
			NumeroDocumento = values[6];
			RowId = values[7];
		}
	}
}
