﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimParsers.POCO.TCC_TelaDoCarrinhoView
{
	public class AtributosItem
	{
		public string NomeAtributo { get; set; }
		public string ValorAtributo { get; set; }
		public string RowId { get; set; }

		public AtributosItem()	{ }

		public AtributosItem(string[] values)
		{
			NomeAtributo = values[0];
			ValorAtributo = values[1];
			RowId = values[2];
		}
	}
}
