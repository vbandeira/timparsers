﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimParsers.POCO.TCC_TelaDoCarrinhoView
{
	public class Ordem: IOrdem
	{
		public string Unknown1 { get; set; }
		public string Unknown2 { get; set; }
		public string Unknown3 { get; set; }
		public string Unknown4 { get; set; }
		public string Unknown5 { get; set; }
		public string Unknown6 { get; set; }
		public string Unknown7 { get; set; }
		public string Unknown8 { get; set; }
		public string Unknown9 { get; set; }
		public string Unknown10 { get; set; }
		public string Unknown11 { get; set; }
		public string Unknown12 { get; set; }
		public string Unknown13 { get; set; }
		public string Unknown14 { get; set; }
		public string NumeroOrdem { get; set; }
		public string ScoreFinal { get; set; }
		public bool Improcedente { get; set; }
		public bool Indevido { get; set; }
		public string StatusOrdem { get; set; }
		public string ValorPagar { get; set; }
		public string Moeda { get; set; }
		public string MotivoImprocedencia { get; set; }
		public string MatriculaCriador { get; set; }
		public string Data { get; set; }
		public string MotivoIndevido { get; set; }
		public string OrganizacaoCriador { get; set; }
		public string RowId { get; set; }

		public Ordem()	{ }

		public Ordem(string[] value)
		{
			Unknown1 = value[0];
			Unknown2 = value[1];
			Unknown3 = value[2];
			Unknown4 = value[3];
			Unknown5 = value[4];
			Unknown6 = value[5];
			Unknown7 = value[6];
			Unknown8 = value[7];
			Unknown9 = value[8];
			Unknown10 = value[9];
			Unknown11 = value[10];
			Unknown12 = value[11];
			Unknown13 = value[12];
			Unknown14 = value[13];
			NumeroOrdem = value[14];
			ScoreFinal = value[15];
			Improcedente = value[16] == "" ? false : true;
			Indevido = value[17] == "" ? false : true;
			StatusOrdem = value[18];
			ValorPagar = value[19];
			Moeda = value[20];
			MotivoImprocedencia = value[21];
			MatriculaCriador = value[22];
			Data = value[23];
			MotivoIndevido = value[24];
			OrganizacaoCriador = value[25];
			RowId = value[26];
		}
	}
}
