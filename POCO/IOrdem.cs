﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimParsers.POCO
{
	public interface IOrdem
	{
		string NumeroOrdem { get; set; }
		string StatusOrdem { get; set; }
		string ValorPagar { get; set; }
		string Data { get; set; }
	}
}
