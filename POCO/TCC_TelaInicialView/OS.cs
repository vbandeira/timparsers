﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimParsers.POCO.TCC_TelaInicialView
{
	/// <summary>
	/// Data from "Item da OS" grid from Siebel's TCC_TelaInicialView
	/// </summary>
	public class OS: IOS
	{
		public string DataCriacao { get; set; }
		public string Protocolo { get; set; }
		public string Motivo1 { get; set; }
		public string Motivo2 { get; set; }
		public string Motivo3 { get; set; }
		public string StatusItem { get; set; }
		public string Acao { get; set; }
		public string NomeProduto { get; set; }
		public string CanalEntrada { get; set; }
		public string DataFechamento { get; set; }
		public string Acesso { get; set; }
		public string Solicitante { get; set; }
		public string NomeCriador { get; set; }
		public string MatriculaCriador { get; set; }
		public string OrganizacaoCriador { get; set; }
		public string RowId { get; set; }

		public OS()	{}

		public OS(string[] values)
		{
			DataCriacao = values[0];
			Protocolo = values[1];
			Motivo1 = values[2];
			Motivo2 = values[3];
			Motivo3 = values[4];
			StatusItem = values[5];
			Acao = values[6];
			NomeProduto = values[7];
			CanalEntrada = values[8];
			DataFechamento = values[9];
			Acesso = values[10];
			Solicitante = values[11];
			NomeCriador = values[12];
			MatriculaCriador = values[13];
			OrganizacaoCriador = values[14];
			RowId = values[15];
		}
	}
}
