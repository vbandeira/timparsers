﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimParsers.POCO.TCC_TelaInicialView
{
	/// <summary>
	/// TCCTelaInicialView class that contains informations of this view
	/// </summary>
	public class TCCTelaInicialView
	{
		public List<ClienteAcesso> ClienteAcesso { get; set; } = new List<TCC_TelaInicialView.ClienteAcesso>();
		public List<ReqServico> ReqServico { get; set; } = new List<TCC_TelaInicialView.ReqServico>();
		public List<OS> OS { get; set; } = new List<TCC_TelaInicialView.OS>();

		public TCCTelaInicialView(List<string[]> data)
		{
			foreach (var item in data)
			{
				switch (item.Count().ToString())
				{
					case "45":
						ClienteAcesso.Add(new TCC_TelaInicialView.ClienteAcesso(item));
						break;
					case "16":
						OS.Add(new TCC_TelaInicialView.OS(item));
						break;
					case "30":
						ReqServico.Add(new TCC_TelaInicialView.ReqServico(item));
						break;
					default:
						break;
				}
			}
		}
	}
}
