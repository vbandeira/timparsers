﻿using CelularDireto.Collections.Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimParsers.POCO.TCC_TelaInicialView
{
	/// <summary>
	/// Data from "Req. Serviço" grid from Siebel's TCC_TelaInicialView
	/// </summary>
	public class ReqServico:IReqServico
	{
		public string Top10 { get; set; }
		public string DataCriacao {get;set;}
		public string Protocolo	{get;set;}
		public string Motivo1 {get;set;}
		public string Motivo2 {get;set;}
		public string Motivo3 {get;set;}
		public string Status {get;set;}
		public string Reincidencia {get;set;}
		public string Insistencia {get;set;}
		public string Descricao	{get;set;}
		public string Notas	{get;set;}
		public string CanalEntrada {get;set;}
		public string DataFechamento {get;set;}
		public string PrazoAnatel {get;set;}
		public string Acesso {get;set;}
		public string Solicitante {get;set;}
		public string NomeCliente {get;set;}
		public string NomeCriador {get;set;}
		public string MatriculaCriador {get;set;}
		public string OrganizacaoCriador {get;set;}
		public string NomeResponsavelTratamento	{get;set;}
		public string MatriculaResponsavelTratamento {get;set;}
		public string OrganizacaoResponsavelTratamento {get;set;}
		public string Prioridade {get;set;}
		public string SemaforoAnatel {get;set;}
		public string SemaforoAnatelB {get;set;}
		public string SemaforoTIM {get;set;}
		public string PrazoTIM {get;set;}
		public string PrazoAnatelB {get;set;}
		public string Resultado {get;set;}
		public string RowId {get;set;}

		public ReqServico()	{}

		public ReqServico(string[] values)
		{
			Top10 = values[0];
			DataCriacao = values[1];
			Protocolo = values[2];
			Motivo1 = values[3];
			Motivo2 = values[4];
			Motivo3 = values[5];
			Status = values[6];
			Reincidencia = values[7];
			Insistencia = values[8];
			Descricao = values[9];
			Notas = values[10];
			CanalEntrada = values[11];
			DataFechamento = values[12];
			PrazoAnatel = values[13];
			Acesso = values[14];
			Solicitante = values[15];
			NomeCliente = values[16];
			NomeCriador = values[17];
			MatriculaCriador = values[18];
			OrganizacaoCriador = values[19];
			NomeResponsavelTratamento = values[20];
			MatriculaResponsavelTratamento = values[21];
			OrganizacaoResponsavelTratamento = values[22];
			Prioridade = values[23];
			SemaforoAnatel = values[24];
			SemaforoAnatelB = values[25];
			SemaforoTIM = values[26];
			PrazoTIM = values[27];
			PrazoAnatelB = values[28];
			Resultado = values[29];
			RowId = values[30];
		}
	}
}
