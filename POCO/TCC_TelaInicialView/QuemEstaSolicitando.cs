﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiebelParser.Domain
{
	public class QuemEstaSolicitando
	{
		public bool Titular { get; set; }
		public string Contato { get; set; }
		public string Outros { get; set; }
	}
}
