﻿using CelularDireto.Collections.Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimParsers.POCO.TCC_TelaInicialView
{
	/// <summary>
	/// ClienteAcesso POCO class from Siebel's TCC_TelaInicialView
	/// </summary>
	public class ClienteAcesso
	{
		/// <summary>
		/// Data from "Quem Está Solicitando" frame of view's form
		/// </summary>
		public class _quemEstaSolicitando
		{
			public bool Titular { get; set; }
			public string Contato { get; set; }
			public string Outros { get; set; }
		}

		/// <summary>
		/// Data from "Dados do Cliente" frame of view's form
		/// </summary>
		public class _dadosCliente: ICliente
		{
			public string Titular { get; set; }
			public string Status { get; set; }
			public string TipoCliente { get; set; }
			public string CPFCNPJ { get; set; }
			public string ClienteDesde { get; set; }
			public string SegmentoMkt { get; set; }
			public bool ClientePossuiCampanha { get; set; }
			public string ClienteCritico { get; set; }
		}

		/// <summary>
		/// Data from "Dados do Acesso" frame of view's form
		/// </summary>
		public class _dadosAcesso: IAcesso
		{
			public string NumAcesso { get; set; }
			public string Status { get; set; }
			public string SegMKT { get; set; }
			public string Tipo { get; set; }
			public string MotivoStatus { get; set; }
			public string SegCaring { get; set; }
			public string Plano { get; set; }
			public string StatusDesde { get; set; }
			public bool AcessoPossuiCampanha { get; set; }
			public string NumeroTemporario { get; set; }
			public string PUK1 { get; set; }
			public bool Instalado { get; set; }
			public string Portabilidade { get; set; }
			public string RowId { get; set; }
		}

		/// <summary>
		/// Data from "Dados da Recarga" frame of view's form
		/// </summary>
		public class _dadosRecarga
		{
			public string Saldo { get; set; }
			public string NumeroSMSTarifado { get; set; }
			public string UltimaRecarga { get; set; }
			public string DataUltimaRecarga { get; set; }
			public string Desativacao { get; set; }
			public string Expiração { get; set; }
			public string Bonus { get; set; }
			public string Bonus2Megabytes { get; set; }
			public string BonusSegundos { get; set; }
			public string BonusSMSOffNet { get; set; }
			public string TrafegoDados { get; set; }
			public string DataInstalacao { get; set; }
			public string ExpiracaoBonus { get; set; }
			public string ExpiracaoBonus2Megabytes { get; set; }
			public string ExpiracaoBonusSegundos { get; set; }
			public string ExpiracaoBonusSMSOffNet { get; set; }
			public string FranquiaVoz { get; set; }
			public string DataExpiracaoFranquiaVoz { get; set; }
		}

		public _quemEstaSolicitando QuemEstaSolicitando { get; set; } = new _quemEstaSolicitando();
		public _dadosCliente DadosCliente { get; set; } = new _dadosCliente();
		public _dadosAcesso DadosAcesso { get; set; } = new _dadosAcesso();
		public _dadosRecarga DadosRecarga { get; set; } = new _dadosRecarga();

		public ClienteAcesso()
		{

		}

		public ClienteAcesso(string[] values)
		{
			QuemEstaSolicitando = new _quemEstaSolicitando
			{
				Titular = values[0] == "Y" ? true : false,
				Contato = values[7],
				Outros = values[13]
			};

			DadosCliente = new _dadosCliente
			{
				ClienteCritico = values[20],
				ClienteDesde = values[14],
				ClientePossuiCampanha = values[19] == "Sim" ? true : false,
				CPFCNPJ = values[9],
				SegmentoMkt = values[15],
				Status = values[3],
				TipoCliente = values[8],
				Titular = values[1]
			};

			DadosAcesso = new _dadosAcesso
			{
				NumAcesso = values[4],
				AcessoPossuiCampanha = values[18] == "Sim" ? true : false,
				Instalado = values[25] == "N" ? false : true,
				MotivoStatus = values[11],
				NumeroTemporario = values[21],
				RowId = values[44]
			};

			DadosRecarga = new _dadosRecarga
			{
				Bonus = values[32],
				Bonus2Megabytes = values[33],
				BonusSegundos = values[34],
				BonusSMSOffNet = values[35],
				DataExpiracaoFranquiaVoz = values[43],
				DataInstalacao = values[37],
				DataUltimaRecarga = values[29],
				Desativacao = values[30],
				ExpiracaoBonus = values[38],
				ExpiracaoBonus2Megabytes = values[39],
				ExpiracaoBonusSegundos = values[40],
				ExpiracaoBonusSMSOffNet = values[35],
				Expiração = values[31],
				FranquiaVoz = values[42],
				NumeroSMSTarifado = values[27],
				Saldo = values[26],
				TrafegoDados = values[36],
				UltimaRecarga = values[28]
			};
		}
	}
}
