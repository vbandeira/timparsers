﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimParsers.Tools
{
	public static class Util
	{
		public static bool validarCPF(string CPF)
		{
			var mt1 = new int[9] { 10, 9, 8, 7, 6, 5, 4, 3, 2 };
			var mt2 = new int[10] { 11, 10, 9, 8, 7, 6, 5, 4, 3, 2 };
			string TempCPF;
			string Digito;
			int soma;
			int resto;

			CPF = CPF.Trim();
			CPF = CPF.Replace(".", "").Replace("-", "");

			if (CPF.Length != 11)
				return false;

			TempCPF = CPF.Substring(0, 9);
			soma = 0;
			for (int i = 0; i < 9; i++)
				soma += int.Parse(TempCPF[i].ToString()) * mt1[i];

			resto = soma % 11;
			resto = resto < 2 ? 0 : 11 - resto;

			Digito = resto.ToString();
			TempCPF = TempCPF + Digito;
			soma = 0;

			for (int i = 0; i < 10; i++)
				soma += int.Parse(TempCPF[i].ToString()) * mt2[i];

			resto = soma % 11;
			resto = resto < 2 ? 0 : 11 - resto;

			Digito = Digito + resto.ToString();

			return CPF.EndsWith(Digito);
		}
	}
}
