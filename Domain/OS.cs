﻿using TimParsers.POCO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace TimParsers.Domain
{
	/// <summary>
	/// OS domain class
	/// </summary>
	public class OS: BaseDomainClass
	{
		public string DataCriacao { get; set; }
		public string Protocolo { get; set; }
		public string Motivo1 { get; set; }
		public string Motivo2 { get; set; }
		public string Motivo3 { get; set; }
		public string StatusItem { get; set; }
		public string Acao { get; set; }
		public string NomeProduto { get; set; }
		public string CanalEntrada { get; set; }
		public string DataFechamento { get; set; }
		public string Acesso { get; set; }
		public string Solicitante { get; set; }
		public string NomeCriador { get; set; }
		public string MatriculaCriador { get; set; }
		public string OrganizacaoCriador { get; set; }

		public OS() { }

		/// <summary>
		/// Imports from POCO object
		/// </summary>
		/// <param name="externalObject">POCO object that implements IOS</param>
		/// <returns></returns>
		public override bool Import(object externalObject)
		{
			if (!(externalObject is IOS))
				throw new ArgumentException($"Import only allowed for object of type {nameof(IOS)}");

			return base.Import(externalObject);

		}
	}
}
