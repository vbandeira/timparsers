﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimParsers.Domain
{
	public class SiebelContext
	{
		public Acesso Acesso { get; set; } = new Acesso();
		public List<AtributosItem> AtributosItem { get; set; } = new List<AtributosItem>();
		public List<BilhetesPortabilidade> BilhetesPortabilidade { get; set; } = new List<BilhetesPortabilidade>();
		public Cliente Cliente { get; set; } = new Cliente();
		public List<DadosInteracaoAlcada> DadosInteracaoAlcada { get; set; } = new List<DadosInteracaoAlcada>();
		public List<ItensOrdem> ItensOrdem { get; set; } = new List<ItensOrdem>();
		public List<LinhasCliente> LinhasCliente { get; set; } = new List<LinhasCliente>();
		public List<Ordens> Ordens { get; set; } = new List<Ordens>();
		public List<OS> OS { get; set; } = new List<OS>();
		public List<Proponi> Proponi { get; set; } = new List<Proponi>();
		public List<ReqServico> ReqServico { get; set; } = new List<ReqServico>();
	}
}
