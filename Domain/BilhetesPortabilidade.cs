﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimParsers.Domain
{
	public class BilhetesPortabilidade: BaseDomainClass
	{
		public string BilheteTemporario { get; set; }
		public string BilhetePortabilidade { get; set; }
		public string LinhaTemporaria { get; set; }
		public string LinhaPortada { get; set; }
		public string StatusBilhete { get; set; }
		public string TipoLinha { get; set; }
		public string NomeCliente { get; set; }
		public string NumeroDocumento { get; set; }
		public string TelefoneContato { get; set; }
		public string OperadoraDoadora { get; set; }
		public string DataPortabilidade { get; set; }
		public string BuscaData { get; set; }
		public string DataSolicitacao { get; set; }
		public string DataReagendamento { get; set; }
		public string DataDesistencia { get; set; }
		public string DataReal { get; set; }
		public string MotivoCancelamento { get; set; }
		public string CEP { get; set; }
		public string CNL { get; set; }
		public string TipoLogradouro { get; set; }
		public string Logradouro { get; set; }
		public string Cidade { get; set; }
		public string Bairro { get; set; }
		public string Estado { get; set; }
		public string Complemento { get; set; }
		public string MotivoRecusa { get; set; }
		public string AutenticacaoEA { get; set; }
		public string FraudeErro { get; set; }
		public string BilheteFraudeErro { get; set; }
		public string JustificativaBilheteFraudeErro { get; set; }
		public string Surfing { get; set; }
		public string StatusPortabilidade { get; set; }
		public string TipoPortabilidade { get; set; }

		public BilhetesPortabilidade() {}

		public override bool Import(object externalObject)
		{
			if (!(externalObject is POCO.TCC_TelaDoCarrinhoView.BilhetesPortabilidade))
				throw new ArgumentException($"Import only allowed for objects of type {nameof(POCO.TCC_TelaDoCarrinhoView.BilhetesPortabilidade)}.");
			return base.Import(externalObject);
		}
	}
}
