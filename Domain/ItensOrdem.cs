﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimParsers.Domain
{
	public class ItensOrdem: BaseDomainClass
	{
		public string Unknown1 { get; set; }
		public string Unknown2 { get; set; }
		public string Unknown3 { get; set; }
		public string NomeProduto { get; set; }
		public string Acao { get; set; }
		public string NumeroAtual { get; set; }
		public string StatusItem { get; set; }
		public string TaxaAdesao { get; set; }
		public string AcoesSolicitadas { get; set; }
		public string BilhetePortabilidadeTemporario { get; set; }
		public string LinhaPortada { get; set; }
		public string StatusPortabilidade { get; set; }
		public string FlagAlcada { get; set; }
		public string NovoTIMChip { get; set; }
		public string TIMChipAtual { get; set; }
		public string NumeroASerDivulgado { get; set; }
		public string EnderecoInstalacao { get; set; }
		public string Unknown5 { get; set; }
		public string Unknown6 { get; set; }
		public string Unknown7 { get; set; }
		public string Unknown8 { get; set; }

		public ItensOrdem()	{}

		public override bool Import(object externalObject)
		{
			if (!(externalObject is POCO.TCC_TelaDoCarrinhoView.ItensOrdem))
				throw new ArgumentException($"Import only allowed for objects of type {nameof(POCO.TCC_TelaDoCarrinhoView.ItensOrdem)}.");
			return base.Import(externalObject);
		}
	}
}
