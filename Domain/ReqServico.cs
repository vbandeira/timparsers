﻿using TimParsers.POCO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimParsers.Domain
{
	/// <summary>
	/// ReqServico domain class
	/// </summary>
	public class ReqServico: BaseDomainClass
	{
		public string Top10 { get; set; }
		public string DataCriacao { get; set; }
		public string Protocolo { get; set; }
		public string Motivo1 { get; set; }
		public string Motivo2 { get; set; }
		public string Motivo3 { get; set; }
		public string Status { get; set; }
		public string Reincidencia { get; set; }
		public string Insistencia { get; set; }
		public string Descricao { get; set; }
		public string Notas { get; set; }
		public string CanalEntrada { get; set; }
		public string DataFechamento { get; set; }
		public string PrazoAnatel { get; set; }
		public string Acesso { get; set; }
		public string Solicitante { get; set; }
		public string NomeCliente { get; set; }
		public string NomeCriador { get; set; }
		public string MatriculaCriador { get; set; }
		public string OrganizacaoCriador { get; set; }
		public string NomeResponsavelTratamento { get; set; }
		public string MatriculaResponsavelTratamento { get; set; }
		public string OrganizacaoResponsavelTratamento { get; set; }
		public string Prioridade { get; set; }
		public string SemaforoAnatel { get; set; }
		public string SemaforoAnatelB { get; set; }
		public string SemaforoTIM { get; set; }
		public string PrazoTIM { get; set; }
		public string PrazoAnatelB { get; set; }
		public string Resultado { get; set; }

		public ReqServico() { }

		/// <summary>
		/// Imports from POCO object
		/// </summary>
		/// <param name="externalObject">POCO object that implements IReqServico</param>
		/// <returns></returns>
		public override bool Import(object externalObject)
		{
			if (externalObject is IReqServico)
				return base.Import(externalObject);
			else
				throw new ArgumentException($"Import only allowed for objects of type {nameof(IReqServico)}.");
		}

	}
}
