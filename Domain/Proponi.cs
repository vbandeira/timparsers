﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimParsers.Domain
{
	public class Proponi: BaseDomainClass
	{
		public string Oferta { get; set; }
		public string NumeroLinha { get; set; }
		public string StatusProponi { get; set; }
		public string DataInicio { get; set; }
		public string DataFim { get; set; }
		public string Produtos { get; set; }
		public string DescricaoCampanha { get; set; }
		public string CodigoCampanha { get; set; }
		public string DescricaoPrograma { get; set; }
		public string TipoCampanha { get; set; }
		public string DataAlteracao { get; set; }
		public string CodigoPDV { get; set; }
		public string Login { get; set; }
		public string StatusOferta { get; set; }

		public Proponi() {}

		public override bool Import(object externalObject)
		{
			if (!(externalObject is POCO.TCC_TelaRelacionamentoView.Proponi))
				throw new ArgumentException($"Import only allowed for object of type {nameof(POCO.TCC_TelaRelacionamentoView.Proponi)}");

			return base.Import(externalObject);

		}
	}
}
