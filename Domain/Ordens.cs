﻿using TimParsers.POCO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimParsers.Domain
{
	public class Ordens: BaseDomainClass
	{
		public string Unknown1 { get; set; }
		public string Unknown2 { get; set; }
		public string Unknown3 { get; set; }
		public string Unknown4 { get; set; }
		public string Unknown5 { get; set; }
		public string Unknown6 { get; set; }
		public string Unknown7 { get; set; }
		public string Unknown8 { get; set; }
		public string Unknown9 { get; set; }
		public string Unknown10 { get; set; }
		public string Unknown11 { get; set; }
		public string Unknown12 { get; set; }
		public string Unknown13 { get; set; }
		public string Unknown14 { get; set; }
		public string NumeroOrdem { get; set; }
		public string ScoreFinal { get; set; }
		public bool Improcedente { get; set; }
		public bool Indevido { get; set; }
		public string StatusOrdem { get; set; }
		public string ValorPagar { get; set; }
		public string Moeda { get; set; }
		public string MotivoImprocedencia { get; set; }
		public string MatriculaCriador { get; set; }
		public string Data { get; set; }
		public string MotivoIndevido { get; set; }
		public string OrganizacaoCriador { get; set; }

		public string PDVResponsavel { get; set; }
		public string LoginResponsavel { get; set; }
		public string ProcuradorRepresentanteLegal { get; set; }
		public string DataUltimaAtualizacao { get; set; }

		public Ordens()	{}

		public override bool Import(object externalObject)
		{
			if (!(externalObject is IOrdem))
				throw new ArgumentException($"Import only allowed for object of type {nameof(IOrdem)}");

			return base.Import(externalObject);

		}
	}
}
