﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimParsers.Domain
{
	public class Plano
	{
		public string DisplayDescription { get; set; }
		public string ProductId { get; set; }
		public string PortItemId { get; set; }
		public string CxOrigId { get; set; }
		public string CxObjName { get; set; }
	}
}
