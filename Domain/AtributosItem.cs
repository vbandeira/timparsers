﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimParsers.Domain
{
	public class AtributosItem: BaseDomainClass
	{
		public string NomeAtributo { get; set; }
		public string ValorAtributo { get; set; }

		public AtributosItem()	{ }

		public override bool Import(object externalObject)
		{
			if (!(externalObject is POCO.TCC_TelaDoCarrinhoView.AtributosItem))
				throw new ArgumentException($"Import only allowed for objects of type {nameof(POCO.TCC_TelaDoCarrinhoView.AtributosItem)}.");
			return base.Import(externalObject);
		}
	}
}
