﻿using TimParsers.POCO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimParsers.Domain
{
	/// <summary>
	/// Acesso domain class
	/// </summary>
	public class Acesso: BaseDomainClass
	{
		public string NumAcesso { get; set; }
		public string Status { get; set; }
		public string SegMKT { get; set; }
		public string Tipo { get; set; }
		public string MotivoStatus { get; set; }
		public string SegCaring { get; set; }
		public string Plano { get; set; }
		public string StatusDesde { get; set; }
		public bool AcessoPossuiCampanha { get; set; }
		public string NumeroTemporario { get; set; }
		public string PUK1 { get; set; }
		public bool Instalado { get; set; }
		public string Portabilidade { get; set; }

		public Acesso()	{}

		/// <summary>
		/// Imports from POCO object
		/// </summary>
		/// <param name="externalObject">POCO object that implements IAcesso</param>
		/// <returns></returns>
		public override bool Import(object externalObject)
		{
			if (externalObject is IAcesso)
				return base.Import(externalObject);
			else
				throw new ArgumentException($"Import only allowed for objects of type {nameof(IAcesso)}.");
		}

	}
}
