﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimParsers.Domain
{
	/// <summary>
	/// BaseDomain class
	/// </summary>
	public abstract class BaseDomainClass
	{
		public string RowId { get; set; }
		public virtual bool Import(object externalObject)
		{
			var externalProperties = externalObject.GetType().GetProperties();

			foreach (var p in externalProperties)
			{
				var value = p.GetValue(externalObject);
				this.GetType().GetProperty(p.Name).SetValue(this, value);
			}

			return true;
		}
	}
}
