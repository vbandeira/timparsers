﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimParsers.Domain
{
	public class LinhasCliente:BaseDomainClass
	{
		public string TimChipICCID { get; set; }
		public string Numero { get; set; }
		public string DescricaoProduto { get; set; }
		public string TipoProduto { get; set; }
		public string NumeroFixoAlternativo { get; set; }
		public string Plano { get; set; }
		public string Status { get; set; }
		public string DataAtivacao { get; set; }
		public string PDVAtivacao { get; set; }
		public string Acao { get; set; }
		public string Motivo { get; set; }
		public string MotivoAcaoAnterior { get; set; }
		public string Unknown1 { get; set; }
		public string Unknown2 { get; set; }
		public string Unknown3 { get; set; }
		public string Unknown4 { get; set; }

		public LinhasCliente()	{}

		public override bool Import(object externalObject)
		{
			if (!(externalObject is POCO.TCC_TelaRelacionamentoView.LinhasCliente))
				throw new ArgumentException($"Import only allowed for object of type {nameof(POCO.TCC_TelaRelacionamentoView.LinhasCliente)}");

			return base.Import(externalObject);

		}
	}
}
