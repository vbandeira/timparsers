﻿using TimParsers.POCO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimParsers.Domain
{
	/// <summary>
	/// Cliente domain class
	/// </summary>
	public class Cliente: BaseDomainClass
	{
		public string Titular { get; set; }
		public string Status { get; set; }
		public string TipoCliente { get; set; }
		public string CPFCNPJ { get; set; }
		public string ClienteDesde { get; set; }
		public string SegmentoMkt { get; set; }
		public bool ClientePossuiCampanha { get; set; }
		public string ClienteCritico { get; set; }
		public string TipoDocumento { get; set; }
		public string TelefoneContato { get; set; }
		public string PossuiDeficiencia { get; set; }
		public string NumeroDocumento { get; set; }
		public string Endereco { get; set; }
		public string DataNascimento { get; set; }
		public string NomeMae { get; set; }
		public string Sexo { get; set; }
		public string MarcacaoSobAnalise { get; set; }

		public Cliente()
		{

		}

		/// <summary>
		/// Imports from POCO object
		/// </summary>
		/// <param name="externalObject">POCO object that implements ICliente</param>
		/// <returns></returns>
		public override bool Import(object externalObject)
		{
			if (!(externalObject is ICliente))
				throw new ArgumentException($"Import only allowed for objects of type {nameof(ICliente)}.");
			return base.Import(externalObject);
		}
	}
}
