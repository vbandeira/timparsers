﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimParsers.Domain
{
	public class DadosInteracaoAlcada: BaseDomainClass
	{
		public string CodigoInteracao { get; set; }
		public string CriadoEm { get; set; }
		public string Tipo { get; set; }
		public string Status { get; set; }
		public string FlagPendencia { get; set; }
		public string DataHoraSolicitacao { get; set; }
		public string LoginSolicitante { get; set; }
		public string JustificativaSolicitante { get; set; }
		public string StatusSolicitacao { get; set; }
		public string NotasSolicitante { get; set; }
		public string DataLimiteAprovacao { get; set; }
		public string LoginAprovador { get; set; }
		public string NomeResponsavelAprovacao { get; set; }

		public DadosInteracaoAlcada() {}

		public override bool Import(object externalObject)
		{
			if (!(externalObject is POCO.TCC_TelaRelacionamentoView.DadosInteracaoAlcada))
				throw new ArgumentException($"Import only allowed for object of type {nameof(POCO.TCC_TelaRelacionamentoView.DadosInteracaoAlcada)}");

			return base.Import(externalObject);

		}
	}
}
